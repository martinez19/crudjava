package com.app.pacientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudClinicaStringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudClinicaStringBootApplication.class, args);
	}

}
