package com.app.pacientes.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;
import com.app.pacientes.servicio.PacientesServicio;


@Controller
public class PacienteControlador {
	
	@Autowired
	private PacientesServicio servicio;
	
	@GetMapping({"/pacientes", "/"})
	public String mostrarFormularioRegistrarPaciente(Model modelo) {
		modelo.addAttribute("pacientes",servicio.listarTodosLosPacientes());
		return "pacientes";
	}
	
	
	
}
