package com.app.pacientes.repositorio;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.web.entidad.Pacientes;


@Repository
public interface PacienteRepositorio extends JpaRepository<Pacientes, Long>  {
	
	
	
}
