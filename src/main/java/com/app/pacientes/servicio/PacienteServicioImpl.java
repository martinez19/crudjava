package com.app.pacientes.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.pacientes.repositorio.PacienteRepositorio;
import com.app.web.entidad.Pacientes;

@Service
public class PacienteServicioImpl implements PacientesServicio {
	
	@Autowired
	private PacienteRepositorio repositorio;
	
	@Override
	public List<Pacientes> listarTodosLosPacientes() {
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}

	@Override
	public Pacientes guardarPaciente(Pacientes paciente) {
		// TODO Auto-generated method stub
		return repositorio.save(paciente);
	}

	@Override
	public Pacientes obtenerPacientePorId(Long id) {
		// TODO Auto-generated method stub
		return repositorio.findById(id).get();
	}

	
	@Override
	public Pacientes actualizarPaciente(Pacientes paciente) {
		// TODO Auto-generated method stub
		return repositorio.save(paciente);
	}

	@Override
	public void  eliminarPaciente(Long id) {
		// TODO Auto-generated method stub
		repositorio.deleteById(id);
	}

}
