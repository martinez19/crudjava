package com.app.pacientes.servicio;

import java.util.List;

import com.app.web.entidad.Pacientes;

public interface PacientesServicio {
		
	public List<Pacientes> listarTodosLosPacientes();
	public Pacientes guardarPaciente(Pacientes paciente);
	public Pacientes obtenerPacientePorId(Long id);
	public Pacientes actualizarPaciente(Pacientes paciente);
	public void eliminarPaciente(Long id);
	
}
